using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Persistent : MonoBehaviour {
    public int manaCount = 0;
    public bool canJump;
    public bool canDoubleJump;
    public bool canDash;
    public bool canSlide;

    public int storyProgress;

    private static Persistent persistent;
    public bool[] isTaken = new bool[30];

    void Awake() {
        if (persistent == null) {
            persistent = this;
            DontDestroyOnLoad(this);
        } else {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }

    public void ReloadScene(float delay) {
        StartCoroutine(WaitReloadScene(delay));
    }

    private IEnumerator WaitReloadScene(float delay) {
        if (delay > 0f)
            yield return new WaitForSeconds(delay);

        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadScene("Cave");
    }
}
