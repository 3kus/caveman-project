using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    private Powerup[] items;
    Persistent persistent;

    void Start()
    {
        persistent = FindObjectOfType<Persistent>();
        items = GetComponentsInChildren<Powerup>();

        for(int i = 0; i<items.Length; i++)
        {
            items[i].SetIndex(i);
            items[i].gameObject.SetActive(!persistent.isTaken[i]);
            Debug.Log(items[i].name + " is index " + i);
        }
    }

    public void SetIsTaken(int index)
    {
        persistent.isTaken[index] = true;
    }
}
