using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour {
    public enum Type {
		Mana,
		Jump,
		DoubleJump,
		Dash,
		Slide
	}

	public Type type;
	public string text;
	public HUDUpdate hud;
	private int index;

	Persistent persistent;
	Protag protag;

	public void SetIndex(int idx)
    {
		index = idx;
    }
	public int getIndex()
	{
		return index;
	}

	private void OnTriggerEnter(Collider other) {
		if (!persistent)
			persistent = FindObjectOfType<Persistent>();

		protag = other.GetComponent<Protag>();

		if (!protag)
			return;

		switch (type) {
			case Type.Mana:
				AddMana();
				break;
			case Type.Jump:
				AddJump();
				break;
			case Type.DoubleJump:
				AddDoubleJump();
				break;
			case Type.Dash:
				AddDash();
				break;
			case Type.Slide:
				AddSlide();
				break;
		}

		if(type != Type.Mana)
			hud.ShowHint(text);

		GetComponentInParent<ItemManager>().SetIsTaken(index);
		Destroy(gameObject);
	}

	void AddMana() {
		persistent.manaCount++;
	}

	void AddJump() {
		persistent.canJump = true;
		protag.canJump = true;
	}

	void AddDoubleJump() {
		persistent.canDoubleJump = true;
		protag.canDoubleJump = true;
	}

	void AddDash() {
		persistent.canDash = true;
		protag.canDash = true;
	}

	void AddSlide() {
		persistent.canSlide = true;
		protag.canSlide = true;
	}
}
