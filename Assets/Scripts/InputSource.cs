using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputSource : MonoBehaviour {
	public virtual float GetHorizontalAxisRaw() {
		return 0f;
	}

	public virtual float GetVerticalAxisRaw()
	{
		return 0f;
	}

	public virtual bool GetJumpButtonDown() {
		return false;
	}

	public virtual bool GetActionButtonDown() {
		return false;
	}

	public virtual bool GetDashButtonDown() {
		return false;
	}

	public virtual bool GetSlideButtonDown() {
		return false;
	}

	public virtual bool GetSlideButtonUp() {
		return false;
	}
}
