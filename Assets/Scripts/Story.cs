using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialog {
	public enum Subject {
		SURTR, FREYJA
	}

	public Subject subject;
	[TextArea(3, 10)]
	public string line;
}

[CreateAssetMenu(fileName = "New Story", menuName = "ScriptableObjects/Story")]
public class Story : ScriptableObject {
	public Dialog[] dialogs;
}
