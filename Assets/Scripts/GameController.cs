using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
	[SerializeField] private CharacterBody cb;
	public Protag protag;

	public CharacterMovement[] activeCharacters;

	Persistent persistent;

	bool reloaded;

	private void Start() {
		persistent = FindObjectOfType<Persistent>();

		cb.OnFallOutOfBounds += ReloadScene;
		protag.OnCrash += ReloadScene;
		protag.GetComponent<Stats>().OnOutOfFire += ReloadScene;
	}

	public void ReloadScene(CharacterBody body) {
		ReloadScene(1f);
	}

	public void ReloadScene() {
		ReloadScene(1f);
	}

	public void ReloadScene(float delay) {
		if (reloaded)
			return;

		reloaded = true;

		persistent.ReloadScene(delay);
	}
}
