using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {
	public Protag protag;

	public float currentFire;
	public float fireDecreaseSpeed = 1f;
	public bool fireDecreasing;

	public event Action OnOutOfFire;

	private void Update() {
		if (fireDecreasing)
			currentFire = currentFire - fireDecreaseSpeed * Time.deltaTime;

		if (!protag.isDead && currentFire < 0f) {
			protag.Dead();
			OnOutOfFire?.Invoke();
		}
	}
}
