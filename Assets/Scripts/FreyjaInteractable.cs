using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreyjaInteractable : MonoBehaviour {
	public DialogManager dialogManager;

	public GameObject indicator;

	bool interacted;

	private void OnTriggerEnter(Collider other) {
		if (!other.CompareTag("Player"))
			return;

		indicator.SetActive(true);
	}

	private void Update() {
		if (indicator.activeSelf && !interacted && Input.GetKeyDown(KeyCode.E)) {
			indicator.SetActive(false);
			interacted = true;
			enabled = false;
			dialogManager.StartLine();
		}
	}

	private void OnTriggerExit(Collider other) {
		if (!other.CompareTag("Player"))
			return;

		indicator.SetActive(false);
	}
}
