using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRotate : MonoBehaviour {
    public float speed = 10f;

	private void Update() {
		transform.Rotate(Vector3.up, Time.deltaTime * speed);
	}
}
