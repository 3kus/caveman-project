using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRunInput : PlayerInput {
	public override float GetHorizontalAxisRaw() {
		return 1f;
	}

	public override float GetVerticalAxisRaw() {
		return Input.GetAxisRaw("Vertical");
	}

	public override bool GetJumpButtonDown() {
		return Input.GetButtonDown("Jump");
	}

	public override bool GetActionButtonDown() {
		return Input.GetButtonDown("Fire1");
	}

	public override bool GetDashButtonDown() {
		return Input.GetKeyDown(KeyCode.LeftShift);
	}

	public override bool GetSlideButtonDown() {
		return Input.GetKeyDown(KeyCode.LeftAlt);
	}

	public override bool GetSlideButtonUp() {
		return Input.GetKeyUp(KeyCode.LeftAlt);
	}
}
