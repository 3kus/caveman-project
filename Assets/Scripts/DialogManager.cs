using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System;
using UnityEngine.SceneManagement;

public class DialogManager : MonoBehaviour {
	public Story[] stories;
	public Sprite surtrSprite;
	public Sprite freyjaSprite;

	public Image overlay;
	public CanvasGroup endCard;

	[Header("DIALOG")]
	public CanvasGroup dialogStuff;
	public Image charSprite;
	public TextMeshProUGUI charName;
	public TextMeshProUGUI lineBox;
	public Button skipBtn;
	public Button nextBtn;

	Queue<Dialog> dialogQ;

	Persistent persistent;
	bool storyInProgress;

	[Header("PROTAG")]
	public Protag protag;

	void ResetDialog() {
		charSprite.enabled = false;
		charName.SetText("");
		lineBox.SetText("");

		skipBtn.interactable = false;
		nextBtn.interactable = false;
	}

	private void Start() {
		persistent = FindObjectOfType<Persistent>();

		if (persistent.storyProgress == 0) {
			StartCoroutine(PrologRoutine());
		} else if (persistent.storyProgress <= 2) {
			StartCoroutine(AfterFirstDeathRoutine());
		} else if (persistent.storyProgress <= 3 && persistent.manaCount >= 10) {
			StartCoroutine(PlotLine1Routine());
		} else if (persistent.storyProgress <= 7 && persistent.manaCount >= 20) {
			StartCoroutine(EndingRoutine());
		}
	}

	public void StartLine() {
		StartCoroutine(StartLineRoutine());
	}

	IEnumerator PrologRoutine() {
		protag.enableInput = false;
		overlay.gameObject.SetActive(true);
		yield return new WaitForSeconds(2f);
		StartStory(0);
		yield return new WaitWhile(() => storyInProgress);
		yield return OverlayFadeOut();
		StartStory(1);
		yield return new WaitWhile(() => storyInProgress);

		persistent.storyProgress = 2;
		SceneManager.LoadScene("Platform 1");
	}

	IEnumerator AfterFirstDeathRoutine() {
		protag.enableInput = false;
		yield return new WaitForSeconds(2f);
		StartStory(2);
		yield return new WaitWhile(() => storyInProgress);
		persistent.storyProgress = 3;
		protag.enableInput = true;
	}

	IEnumerator StartLineRoutine() {
		protag.enableInput = false;
		StartStory(3);
		yield return new WaitWhile(() => storyInProgress);
		SceneManager.LoadScene("Platform 1");
	}

	IEnumerator PlotLine1Routine() {
		protag.enableInput = false;
		yield return new WaitForSeconds(2f);
		StartStory(4);
		yield return new WaitWhile(() => storyInProgress);
		yield return OverlayFadeIn();
		StartStory(5);
		yield return new WaitWhile(() => storyInProgress);
		yield return OverlayFadeOut();
		StartStory(6);
		yield return new WaitWhile(() => storyInProgress);
		persistent.storyProgress = 7;
		protag.enableInput = true;
	}

	IEnumerator EndingRoutine() {
		protag.enableInput = false;
		yield return new WaitForSeconds(2f);
		StartStory(7);
		yield return new WaitWhile(() => storyInProgress);
		yield return OverlayFadeIn();
		StartStory(8);
		yield return new WaitWhile(() => storyInProgress);
		yield return OverlayFadeOut();
		StartStory(9);
		yield return new WaitWhile(() => storyInProgress);
		persistent.storyProgress = 10;
		
		yield return OverlayFadeIn();
		endCard.gameObject.SetActive(true);
		endCard.DOFade(1f, 1f);
	}

	private void StartStory(int index) {
		storyInProgress = true;
		var story = stories[index];
		dialogQ = new Queue<Dialog>(story.dialogs);

		StartCoroutine(StartStoryRoutine());
	}

	IEnumerator StartStoryRoutine() {
		ResetDialog();
		dialogStuff.gameObject.SetActive(true);
		yield return dialogStuff.DOFade(1f, 1f).WaitForCompletion();

		skipBtn.interactable = true;
		nextBtn.interactable = true;

		charSprite.enabled = true;
		Next();
	}

	public void Next() {
		Dialog dialog = null;
		try {
			dialog = dialogQ.Dequeue();
		} catch (InvalidOperationException) {
			EndStory();
			return;
		}

		charSprite.sprite = dialog.subject == Dialog.Subject.SURTR ? surtrSprite : freyjaSprite;
		charName.SetText(dialog.subject == Dialog.Subject.SURTR ? "SURTR" : "FREYJA");
		lineBox.SetText(dialog.line);
	}

	public void Skip() {
		EndStory();
	}

	private void EndStory() {
		StartCoroutine(EndStoryRoutine());
	}

	IEnumerator EndStoryRoutine() {
		skipBtn.interactable = false;
		nextBtn.interactable = false;

		yield return dialogStuff.DOFade(0f, 1f).WaitForCompletion();
		dialogStuff.gameObject.SetActive(false);
		storyInProgress = false;
	}

	#region helper
	IEnumerator OverlayFadeOut() {
		yield return overlay.DOFade(0f, 2f).WaitForCompletion();
		overlay.gameObject.SetActive(false);
	}

	IEnumerator OverlayFadeIn() {
		overlay.gameObject.SetActive(true);
		overlay.DOFade(0f, 0f);
		yield return overlay.DOFade(1f, 2f).WaitForCompletion();
	}
	#endregion
}