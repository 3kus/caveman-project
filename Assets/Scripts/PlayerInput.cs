using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : InputSource {
	public override float GetHorizontalAxisRaw() {
		return Input.GetAxisRaw("Horizontal");
	}
}
