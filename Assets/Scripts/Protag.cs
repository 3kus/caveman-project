using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Protag : CharacterMovement {
	[Header("Protag Parameters")]
	[SerializeField] private DefaultState		defaultState		= new DefaultState();
	[SerializeField] private DashState			dashState			= new DashState();
	[SerializeField] private SlideState			slideState			= new SlideState();

	public bool canDash;
	public bool canSlide;
	public bool isAutoRunning;
	public event Action OnCrash;

	private bool actionAllowed;

	private FiniteStateMachine fsm;

	private CharacterState CurrentState {
		get {
			return fsm.CurrentState as CharacterState;
		}
	}

	private void Awake() {
		OnCharacterCollision += HandleCharacterCollision;

		defaultState.cm =
		dashState.cm =
		slideState.cm = this;
		
		fsm = new FiniteStateMachine(defaultState);

		SetAbility();
	}

	void SetAbility() {
		var persistent = FindObjectOfType<Persistent>();

		canJump = persistent.canJump;
		canDoubleJump = persistent.canDoubleJump;
		canDash = persistent.canDash;
		canSlide = persistent.canSlide;
	}

	protected override void ResetFields() {
		base.ResetFields();

		actionAllowed = false;

		if (fsm.CurrentState != defaultState)
			fsm.Pop();
	}

	protected override void ActionInput() {
		if (Grounded && fsm.CurrentState == defaultState)
			actionAllowed = true;

		if (!actionAllowed)
			return;

		if (canSlide && Grounded && input.GetSlideButtonDown()) {
			actionAllowed = false;
			fsm.Push(slideState);

		} else if (canDash && input.GetDashButtonDown()) {
			actionAllowed = false;
			fsm.Push(dashState);
		}
	}

	protected override void StateInput() {
		CurrentState.HandleInput();
	}

	protected override void Update() {
		base.Update();

		if (Grounded && fsm.CurrentState == defaultState)
			actionAllowed = true;

		fsm.Update();
	}

	private void HandleCharacterCollision(CharacterBody cb) {
		((CharacterState)fsm.CurrentState).HandleCharacterCollision(cb);
	}

	protected override void OnControllerColliderHit(ControllerColliderHit hit) {
		base.OnControllerColliderHit(hit);

		if (!isDead && isAutoRunning && (hit.normal.x <= -0.9f || hit.normal.y <= -0.9f)) {
			isDead = true;
			OnCrash?.Invoke();
		}
	}
}

[System.Serializable]
public class DefaultState : CharacterState {
	[SerializeField] private GameController gc;
	public override void HandleCharacterCollision(CharacterBody cb) {
		cm.Dead();
		gc.ReloadScene(2f);
	}
}

[System.Serializable]
public class DashState : CharacterState
{
	[SerializeField] private float duration;
	[SerializeField] private AnimationCurve speedCurve;

	private float time;

	public override void Enter()
    {
		cm.enableInput = false;
		cm.enableGravity = false;
		cm.SetVerticalVelocity(0f);

		time = 0f;

		//Object.Instantiate(effectPrefab, effectPos);

		cm.animator.SetBool("Dash", true);

		base.Enter();
    }

    public override StateInfo Update()
    {
		float progress = time / duration;
		bool finished = progress > 1;

		if (!finished)
			cm.SetHorizontalSpeed(speedCurve.Evaluate(progress));

		time += Time.deltaTime;
		return new StateInfo(finished);
    }

    public override void Exit()
    {
		cm.enableInput = true;
		cm.enableGravity = true;

		cm.animator.SetBool("Dash", false);

		base.Exit();
    }
}

[System.Serializable]
public class SlideState : CharacterState {
	[SerializeField] private float duration;
	[SerializeField] private AnimationCurve speedCurve;

	public float colliderHeight;
	public Transform visual;

	bool released;

	public override void Enter() {
		cm.enableInput = false;
		//cm.enableGravity = false;
		//cm.SetVerticalVelocity(0f);

		released = false;

		//Object.Instantiate(effectPrefab, effectPos);

		cm.animator.SetBool("Slide", true);
		cm.SetColliderHeight(colliderHeight);
		//visual.localScale = new Vector3(2f, .35f, 1f);

		base.Enter();
	}

	public override void HandleInput() {
		if (cm.input.GetSlideButtonUp()) {
			released = true;
		}
	}

	public override StateInfo Update() {
		return new StateInfo(released);
	}

	public override void Exit() {
		cm.enableInput = true;
		//cm.enableGravity = true;

		cm.animator.SetBool("Slide", false);
		cm.SetColliderHeight();
		//visual.localScale = new Vector3(2f, 2f, 1f);

		base.Exit();
	}
}