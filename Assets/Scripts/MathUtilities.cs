﻿using UnityEngine;

public static class MathUtilities {
	public static Vector2 SnapToAxesNormals(Vector2 v) {
		float xAbs = Mathf.Abs(v.x);
		float yAbs = Mathf.Abs(v.y);

		if (xAbs >= yAbs)
			return v.x >= 0f ? Vector2.right : Vector2.left;
		else
			return v.y >= 0f ? Vector2.up : Vector2.down;
	}

	public static Vector3 SnapToAxesNormals(Vector3 v) {
		float[] abs = { Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z) };

		int idx = 0;
		for (int i = 1; i < abs.Length; i++) {
			if (abs[i] > abs[idx])
				idx = i;
		}

		if (idx == 0)
			return v.x >= 0f ? Vector3.right : Vector3.left;
		else if (idx == 1)
			return v.y >= 0f ? Vector3.up : Vector3.down;
		else
			return v.z >= 0f ? Vector3.forward : Vector3.back;
	}
}

