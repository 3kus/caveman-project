using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public delegate void CharacterAction(CharacterMovement characterMovement);

public class CharacterMovement : CharacterBody {
	[Header("CharacterMovement Parameters")]
	[SerializeField] public InputSource input;

	[SerializeField] public Animator animator;

	[SerializeField] protected float minInputMagnitude = .1f;

	[SerializeField] private float maxSpeed;
	[SerializeField] protected float jumpHeight;

	[SerializeField] private float rotateSpeed;

	private Vector2 directionalInput;
	protected bool jump;

	// 4 possible values: forward, right, back, left
	protected CardinalDirection LocalDirection { get; private set; }
	protected Vector3 LocalDirectionVector => LocalDirection.GetVector3();

	public bool enableInput;
	public bool isDead;

	public bool IsPlayer => input is PlayerInput;

	public bool canJump;
	public bool canDoubleJump;
	public int jumpCount;

	protected override void ResetFields() {
		base.ResetFields();

		directionalInput = Vector2.zero;
		jump = false;

		try {
			UpdateLocalDirection();
		} catch (MissingReferenceException) { // Reload scene problem
			LocalDirection = CardinalDirection.Forward;
		}

		enableInput = true;
		isDead = false;
	}

	protected override void Update() {
		base.Update();

		if (isDead) {
			desiredVelocity = Vector3.zero;
		} else {
			if (enableInput) {
				LocomotionInput();
				JumpInput();
				ActionInput();
			}

			StateInput();
		}
	}

	private void LateUpdate() {
		UpdateAnimation();
		Rotate();
	}

	private void LocomotionInput() {
		directionalInput = new Vector2(input.GetHorizontalAxisRaw(), 0f);
		float mag = directionalInput.magnitude;

		if (mag >= minInputMagnitude) {
			UpdateLocalDirection(true);
			desiredVelocity = new Vector3(LocalDirectionVector.x * mag * maxSpeed, 0f);
		} else {
			desiredVelocity = Vector3.zero;
		}
	}

	protected virtual void JumpInput() {
		if (!canJump)
			return;

		jump = input.GetJumpButtonDown();

		if (jump) {
			if (Grounded) {
				velocity2D.y = Mathf.Sqrt(-2f * JumpGravity.y * jumpHeight);

				jumpCount = 1;

			} else if (canDoubleJump && jumpCount == 1) {
				velocity2D.y = Mathf.Sqrt(-2f * JumpGravity.y * jumpHeight);

				jumpCount = 2;
			}
		}
	}

	protected virtual void ActionInput() {}
	protected virtual void StateInput() { }

	protected virtual void UpdateAnimation() {
		animator.SetFloat("Locomotion", Mathf.Clamp01(directionalInput.magnitude));
		animator.SetFloat("YVelocity", cc.velocity.y);

		float facingRight = (LocalDirectionVector.z + 1f) / 2f;
		animator.SetFloat("FacingRight", facingRight);
		
		animator.SetBool("Jump", jump);
		animator.SetBool("IsGrounded", Grounded);
	}

	protected void UpdateLocalDirection(bool useDirectionalInput = false) {
		Vector3 freeLocalDirection;
		if (!IsPlayer && useDirectionalInput) {
			freeLocalDirection = new Vector3(-directionalInput.y, 0f, directionalInput.x);

		} else {
			Vector3 worldDirection;
			if (useDirectionalInput) {
				worldDirection = Camera.main.transform.TransformDirection(directionalInput.x, 0f, directionalInput.y);
				worldDirection.y = 0f;
				worldDirection.Normalize();
			} else {
				worldDirection = transform.forward;
			}

			freeLocalDirection =
				Quaternion.Inverse(
				Quaternion.LookRotation(
					Vector3.forward)) * worldDirection;
		}

		LocalDirection = CardinalDirectionExtensions.GetCardinalDirection(freeLocalDirection);
	}

	private void Rotate() {
		//Vector3 targetDirection = curvedPath.GetDirectionAtDistance(pathX, LocalDirection);
		Vector3 targetDirection = LocalDirectionVector;

		float singleStep = rotateSpeed * Time.deltaTime;
		Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);

		transform.rotation = Quaternion.LookRotation(newDirection);
	}

	public void SetHorizontalSpeed(float speed) {
		desiredVelocity = new Vector3(LocalDirectionVector.x * speed, desiredVelocity.y);
	}

	public void SetVerticalVelocity(float value) {
		velocity2D.y = value;
	}

	public void Dead() {
		if (isDead)
			return;

		isDead = true;

		//gameObject.layer = LayerMask.NameToLayer("DeadBody");
		animator.SetTrigger("Dead");
	}
}

public abstract class CharacterState : State {
	public CharacterMovement cm;
	[SerializeField] private UnityEvent OnEnter;
	[SerializeField] private UnityEvent OnExit;

	public override void Enter() {
		OnEnter.Invoke();
	}

	public virtual void HandleInput() { }
	public virtual void UpdateAnimation() { }
	public virtual void HandleCharacterCollision(CharacterBody cb) { }
	public override void Exit() {
		OnExit.Invoke();
	}
}