using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CardinalDirection {
	// Global use
	North = 0,
	East = 1,
	South = 2,
	West = 3,

	// Local use
	Forward = 0,
	Right = 1,
	Back = 2,
	Left = 3,

	Up = 0,
	Down = 2
}

public static class CardinalDirectionExtensions {
	public static bool AlignedWithPath(this CardinalDirection dir) {
		return dir == CardinalDirection.Forward || dir == CardinalDirection.Back;
	}

	public static bool OppositeOf(this CardinalDirection dir, CardinalDirection other) {
		return Mathf.Abs(dir - other) == 2;
	}

	public static Vector3 GetVector3(this CardinalDirection dir) {
		switch (dir) {
			case CardinalDirection.Forward:
				return Vector3.forward;
			case CardinalDirection.Right:
				return Vector3.right;
			case CardinalDirection.Back:
				return Vector3.back;
			default: // case CardinalDirection.Left:
				return Vector3.left;
		}
	}

	public static Vector2 GetVector2(this CardinalDirection dir) {
		switch (dir) {
			case CardinalDirection.Up:
				return Vector2.up;
			case CardinalDirection.Right:
				return Vector2.right;
			case CardinalDirection.Down:
				return Vector2.down;
			default: // case CardinalDirection.Left:
				return Vector2.left;
		}
	}

	public static CardinalDirection GetCardinalDirection(Vector3 vector3) {
		Vector3 snapped = MathUtilities.SnapToAxesNormals(vector3);

		if (snapped.x != 0f)
			return snapped.x > 0 ? CardinalDirection.Right : CardinalDirection.Left;
		else if (snapped.y != 0f)
			return snapped.y > 0 ? CardinalDirection.Up : CardinalDirection.Down;
		else // snapped.z != 0f
			return snapped.z > 0 ? CardinalDirection.Forward : CardinalDirection.Back;
	}

	public static CardinalDirection GetCardinalDirection(Vector2 vector2) {
		Vector2 snapped = MathUtilities.SnapToAxesNormals(vector2);

		if (snapped.x != 0f)
			return snapped.x > 0 ? CardinalDirection.Right : CardinalDirection.Left;
		else // snapped.y != 0f
			return snapped.y > 0 ? CardinalDirection.Up : CardinalDirection.Down;
	}
}
