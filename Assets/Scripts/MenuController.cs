using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class MenuController : MonoBehaviour
{
	public string firstScenename;
	public CanvasGroup panel;

	bool clicked;

	private void Start() {
		StartCoroutine(StartMenu());
	}

	IEnumerator StartMenu() {
		panel.alpha = 0f;

		yield return new WaitForSeconds(1f);
		clicked = false;
		yield return panel.DOFade(1f, 2f).WaitForCompletion();
		yield return new WaitUntil(() => clicked);

		yield return panel.DOFade(0f, 2f).WaitForCompletion();

		SceneManager.LoadScene(firstScenename);
	}

	private void Update() {
		if (Input.anyKeyDown) {
			clicked = true;
		}
	}
}
