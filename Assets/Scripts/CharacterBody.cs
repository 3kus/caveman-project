using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBody : MonoBehaviour {
	[Header("CharacterBody Parameters")]
	[SerializeField] protected CharacterController cc;

	[SerializeField] private float upwardGravityModifier, downwardGravityModifier = 1f;
	[SerializeField] private float groundedDistance;
	[SerializeField] private float groundedLeeway;

	protected Vector3 desiredVelocity;
	protected Vector3 velocity2D;
	public bool Grounded { get; protected set; }
	public bool enableGravity;

	protected event Action<Collider> OnColliderCollision;
	protected event Action<CharacterBody> OnCharacterCollision;

	public event Action<CharacterBody> OnFallOutOfBounds;

	private float timeSinceLastActualGrounded;

	private float defaultColliderHeight;
	private float defaultColliderCenterY;

	protected virtual void ResetFields() {
		desiredVelocity = Vector3.zero;
		velocity2D = Vector3.zero;
		Grounded = false;
		enableGravity = true;

		timeSinceLastActualGrounded = 0f;
	}

	protected Vector3 Gravity => velocity2D.y >= 0 ? JumpGravity : FallingGravity;
	protected Vector3 JumpGravity => upwardGravityModifier * Physics.gravity;
	protected Vector3 FallingGravity => downwardGravityModifier * Physics.gravity;

	private bool Ascending => velocity2D.y > 0f;
	private bool Descending => velocity2D.y < 0f;

	private void OnEnable() {
		ResetFields();

		defaultColliderHeight = cc.height;
		defaultColliderCenterY = cc.center.y;
	}

	protected virtual void Update() {
		FallCheck();
		GroundCheck();
		Move();
	}

	private void FixedUpdate() {
	}

	private void LateUpdate() {
		var v = transform.position;
		v.z = 0f;
		transform.position = v;
	}

	private void OnDisable() {
		ResetFields();
	}


	private void FallCheck() {
		if (transform.position.y < -10) {
			gameObject.SetActive(false);
			OnFallOutOfBounds?.Invoke(this);
		}
	}

	private void GroundCheck() {
		bool actualGrounded = cc.isGrounded || Physics.Raycast(transform.position, Vector3.down, groundedDistance);

		if (actualGrounded) {
			timeSinceLastActualGrounded = 0f;
		} else {
			timeSinceLastActualGrounded += Time.deltaTime;
		}

		bool withinLeeway = timeSinceLastActualGrounded <= groundedLeeway;

		Grounded = !Ascending && (actualGrounded || withinLeeway);
	}

	private void Move() {
		//velocity = cc.velocity;
		velocity2D.x = desiredVelocity.x; //???
		if (enableGravity)
			velocity2D += Gravity * Time.deltaTime;

		var diff2D = velocity2D * Time.deltaTime;
		//var desiredNextPos = currentPath.GetPointAtDistance(pathX + diff2D.x, 
		//	curvedPath.BezierPath.IsClosed ? EndOfPathInstruction.Loop : EndOfPathInstruction.Stop);
		//desiredNextPos.y = transform.position.y + diff2D.y;
		var desiredNextPos = transform.position + diff2D;
		var desiredDiff = desiredNextPos - transform.position;

		print(desiredDiff);
		cc.Move(desiredDiff);

		if (Grounded)
			velocity2D.y = 0f;
	}


	protected virtual void OnControllerColliderHit(ControllerColliderHit hit) {
		if (hit.gameObject.TryGetComponent(out CharacterBody cb)) {
			OnCharacterCollision?.Invoke(cb);
			cb.OnCharacterCollision?.Invoke(this);
		} else {
			OnColliderCollision?.Invoke(hit.collider);
		}
	}

	public void SetColliderHeight(float height = -1f) {
		if (height < 0) {
			cc.center = new Vector3(0f, defaultColliderCenterY, 0f);
			cc.height = defaultColliderHeight;

		} else {
			cc.center = new Vector3(0f, height / 2f, 0f);
			cc.height = height;
		}
	}
}
