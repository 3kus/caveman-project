using UnityEngine;
using TMPro;

public class HUDUpdate : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI timerText;

    private Persistent persistent;
    public Stats stats;
    public GameObject hintBG;
    public TextMeshProUGUI hintText;

    private void Start()
    {
        persistent = FindObjectOfType<Persistent>();
    }

    void Update()
    {
        int score = persistent.manaCount;
        int time = (int)stats.currentFire;

        scoreText.text = score.ToString();
        timerText.text = time.ToString();
    }

    public void ShowHint(string newText)
    {
        hintText.text = newText;
        hintBG.SetActive(true);

        Invoke("HideHint", 3f);
    }

    public void HideHint()
    {
        hintBG.SetActive(false);
    }
}
