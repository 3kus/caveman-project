﻿using System.Collections.Generic;
using UnityEngine;

public class FiniteStateMachine
{
	private readonly Stack<State> stack;
	public State CurrentState {
		get {
			return stack.Peek();
		}
	}

	public FiniteStateMachine(State defaultState)
    {
		stack = new Stack<State>();
		stack.Push(defaultState);
    }

	public void Update()
    {
		var info = CurrentState.Update();

		if (info.finished)
        {
			if (info.nextState != null)
            {
				stack.Pop().Exit();
				stack.Push(info.nextState);
				CurrentState.Enter();
            }
			else
            {
				Pop();
            }
        }
    }

	public void Push(State state)
    {
		if (CurrentState == state) return;

		CurrentState.Exit();
		stack.Push(state);
		CurrentState.Enter();
    }

	public void Pop()
	{
		stack.Pop().Exit();
		CurrentState.Enter();
	}
}

public abstract class State
{
	public struct StateInfo
    {
		public bool finished;
		public State nextState;

        public StateInfo(bool finished, State nextState = null)
        {
			this.finished = finished;
			this.nextState = nextState;
        }
    }

	public virtual void Enter() { }
	public virtual void Exit() { }

	public virtual StateInfo Update()
	{
		return new StateInfo(false);
	}
}